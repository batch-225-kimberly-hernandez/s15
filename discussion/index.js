// Comments in Javascript
// - To write comments, we use two forward slash for single line comments and two forward slash and two askterisks for multi line comments. 
// This is single line comment.
/*
This is a multi-line comment.
To write them, we add two askterisks inside of the forward slashes and write comments in between them.
*/


// VARIABLE
/* 
 - Variables contain values that can be changed over the execution time of the program.
 - To delace a variable, we use the "let" keyword.

*/

let productName = 'desktop computer';
console.log(productName);
productName='celphone'
console.log(productName);

let productPrice=500;
console.log(productPrice);
productPrice=450;
console.log(productPrice)

// CONTANTS
/*
- Use contants for values that will not change.
*/

/*const deliveryFee = 30;
console.log(deliveryFee);
*/


// DATA TYPES
// 1. Strings
/*
 - Strings are a series of characters that create a word, a phrase, sentce, or anything related to "TEXT".
 - Strings in Javascript can be written using a single quote ('') or a double quote ("")
 - On other programming languages, only the double quote can be used for creating strings. 
*/

let country = 'Philippines';
let province = "Metro Manila";

// CONCATENATION
console.log(country + ", " + province);

// 2. NUMBERS
/*
 - Include integers / whole numbers, decimal numbers, fraction, exponential notation.
*/

let headCount = 26;
console.log(headCount);

let grade=98.6;
console.log(grade);

let PI = 22/7;
console.log(PI);


console.log(Math.PI);


let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35, 
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails)








